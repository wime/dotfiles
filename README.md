# Dotfiles
These are my dotfiles!

## Purpose
The main purpose of the repo is for personal use. It is a way to store and keep 
track of my own dotfiles. 

Other people are however free to take inspiration or use wathever they like.

## System
* [Sway](https://swaywm.org/)   -   Tiling compositor for Wayland 
* [Waybar](https://github.com/Alexays/Waybar)   -   Highhly customizable panel
* [Fuzzel](https://codeberg.org/dnkl/fuzzel)    -   Minimal application laucher
* [Mako](https://github.com/emersion/mako)  -   Notification daemon
* [wlsunset](https://sr.ht/~kennylevinsen/wlsunset/)    -   Night light
* [Swaylock](https://github.com/swaywm/swaylock)    -   Screenlocker
* pavucontrol

## Terminal
* [Kitty](https://sw.kovidgoyal.net/kitty/)
* [Neovim](http://neovim.io/)
* [fish](https://fishshell.com/)
* [starship](https://starship.rs/)
* [ranger](https://github.com/ranger/ranger)

## Bare repo

dotfiles config --local status.showUntrackedFiles no
