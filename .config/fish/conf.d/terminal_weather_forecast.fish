
function terminal-weather-forecast 
    # Set inputs:
    argparse 'l/location' 'd/days' 's/symbol' -- $argv
    or return

    # Run Python script
    $HOME/.venvs/venv/bin/python $HOME/Projects/terminal-weather-forecast/weather_forecast.py $argv[1] $argv[2] $argv[3] 
end


