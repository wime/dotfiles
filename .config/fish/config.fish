### FISH CONFIG ###
#-----------------#

## EXPORT ###
set fish_vi_keybindings     # Set VI keybindings 
export EDITOR="nvim"        # Export NeoVIM as editor
set fish_greeting           # Disable default fish greeting
set -x PATH $PATH $HOME/bin

## ALIASES ##
# System control: Screenlocking, logout and suspend
alias lock="swaylock -i ~/.config/wallpaper/wallpaper.jpg"
alias logout="swaymsg exit"
alias suspend="systemctl suspend"

# Bare repository
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

# Images in kitty
alias icat="kitty +kitten icat"

# Python environment
alias venv="echo (source ~/.venvs/venv/bin/activate.fish)"

# Utilities
alias calc="$HOME/.venvs/venv/bin/ipython"
alias switch_theme="fish $HOME/.config/system_scripts/theme_switcher.fish"

# Renaming of commands
alias cal="cal -w"          # Show weeknumbers with cal

# Application launchers
alias vi="nvim"             # Lauch NeoVIM with vi
alias wf=terminal-weather-forecast

## FUNCTIONS ##
# Enable/disable night light
function night-light
    if pgrep wlsunset >/dev/null
        killall wlsunset
        notify-send "Night Light is deactivated"
    else
        set NOW echo (date +"%H:%M")
        nohup >/dev/null wlsunset -S 00:00 -s 00:01 -T 6500 -t 5000 & disown
        notify-send "Night Light is activated"
    end
end

## STARSHIP PROMPT ##
starship init fish | source

