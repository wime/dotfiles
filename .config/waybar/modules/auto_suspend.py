import subprocess
import ast
import sys
import json


def write_to_waybar(
    text: str = "", tooltip: str = "", percentage: float = 1, class_: str = ""
) -> None:
    "Write values to Waybar with stdout."
    output = {
        "text": text,
        "tooltip": tooltip,
        "percentage": percentage,
        "class": class_,
    }
    sys.stdout.write(json.dumps(output) + "\n")
    sys.stdout.flush()


def get_output(cmd: str) -> str:
    """Get output of shell command"""
    result = subprocess.run(cmd, shell=True, capture_output=True)
    return result.stdout.decode("utf-8").replace("\n", "")


def main():
    """Write state of autosuspend to waybar."""
    active = get_output("python ~/.bin/settings autosuspend-get")
    active = ast.literal_eval(active)
    class_ = {True: "enabled", False: "disabled"}[active]
    write_to_waybar(tooltip=f"Autosuspend: {class_.capitalize()}", class_=class_)


if __name__ == "__main__":
    main()
