#!/bin/python
import subprocess
import sys
import json


def write_to_waybar(
    text: str = "", tooltip: str = "", percentage: float = 1, class_: str = ""
) -> None:
    "Write values to Waybar with stdout."
    output = {
        "text": text,
        "tooltip": tooltip,
        "percentage": percentage,
        "class": class_,
    }
    sys.stdout.write(json.dumps(output) + "\n")
    sys.stdout.flush()


def get_output(cmd: str) -> str:
    """Get output of shell command"""
    result = subprocess.run(cmd, shell=True, capture_output=True)
    return result.stdout.decode("utf-8").replace("\n", "")


def main():
    """Get media output"""
    title = get_output("playerctl metadata -f '{{title}}'")
    status = get_output("playerctl status").lower()
    title = title.split("(")[0]
    title = title[:30]
    write_to_waybar(
        text=f" {title} ({status})",
        tooltip=f" {title} ({status})",
        percentage={"playing": 1, "paused": 0.5, "stopped": 0}[status],
        class_=status
    )


if __name__ == "__main__":
    main()
