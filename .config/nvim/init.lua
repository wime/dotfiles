-- NEOVIM CONFIG
-- 
-- @author: wime
-- @date: 2022-12-15

------------------------------------------------------------------------------
--VARIABLES
local g   = vim.g
local o   = vim.o
local opt = vim.opt
local A   = vim.api
local Plug = vim.fn['plug#']

------------------------------------------------------------------------------
-- PLUGINS
vim.call('plug#begin', '~/.config/nvim/plugged')

-- Edit
Plug 'neovim/nvim-lspconfig'
Plug 'nvim-treesitter/nvim-treesitter'
Plug 'nvim-lua/completion-nvim'
Plug 'karoliskoncevicius/vim-sendtowindow'
Plug 'windwp/nvim-autopairs'
Plug 'dkarter/bullets.vim'

-- Visual
Plug 'ishan9299/nvim-solarized-lua'
Plug 'wfxr/minimap.vim'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'nvim-tree/nvim-tree.lua'
Plug 'nvim-lualine/lualine.nvim'

vim.call('plug#end')

------------------------------------------------------------------------------
-- BASIC SETTINGS

-- Better editor UI
o.number = true
o.relativenumber = true
o.cursorline = true

-- Better editing experience
o.expandtab = true
o.smarttab = true
o.cindent = true
o.autoindent = true
o.wrap = true
o.textwidth = 300
o.tabstop = 4
o.shiftwidth = 4
o.softtabstop = -1 -- If negative, shiftwidth value is used
opt.colorcolumn = '80'

-- Makes neovim and host OS clipboard play nicely with each other
o.clipboard = 'unnamedplus'

-- Case insensitive searching UNLESS /C or capital in search
o.ignorecase = true
o.smartcase = true

-- Better buffer splitting
o.splitright = true
o.splitbelow = true

------------------------------------------------------------------------------
-- COLOR SCHEME

o.termguicolors = true
A.nvim_command [[colorscheme solarized-flat]]

-- Set light or dark based on terminal-background
vim.api.nvim_create_autocmd("OptionSet", {
	pattern = "background",
	callback = function()
		vim.cmd("set background=" .. (vim.v.option_new == "light" and "light" or "dark"))
	end,
})

------------------------------------------------------------------------------
-- PLUGIN SETTINGS

-- NvimTree
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- LSP
g.completion_enable_fuzzy_match = 1
vim.cmd("set completeopt=menuone,noinsert,noselect")

require("nvim-tree").setup()
require('lualine').setup()
require("nvim-autopairs").setup {}

------------------------------------------------------------------------------
-- CUSTOM FUNCTIONS

function ToggleBackground()
    if o.background == "light" then
        o.background = "dark"
    else
        o.background="light"
    end
end

------------------------------------------------------------------------------
-- KEYBINDINGS

-- Spawning and closing windows
vim.keymap.set('n', '<A-Enter>', '<cmd>vsplit | term<CR>')
vim.keymap.set('n', '<A-S-Enter>', '<cmd>split | term<CR>')
vim.keymap.set('n', '<A-q>', '<cmd>close<CR>')

-- Window Navigation
vim.keymap.set('n', '<A-h>', '<cmd>wincmd h<CR>')
vim.keymap.set('n', '<A-j>', '<cmd>wincmd j<CR>')
vim.keymap.set('n', '<A-k>', '<cmd>wincmd k<CR>')
vim.keymap.set('n', '<A-l>', '<cmd>wincmd l<CR>')
vim.keymap.set('t', '<A-Esc>', '<C-\\><C-n>')

-- Moving windows
vim.keymap.set('n', '<A-S-h>', '<cmd>wincmd H<CR>')
vim.keymap.set('n', '<A-S-j>', '<cmd>wincmd J<CR>')
vim.keymap.set('n', '<A-S-k>', '<cmd>wincmd K<CR>')
vim.keymap.set('n', '<A-S-l>', '<cmd>wincmd L<CR>')

-- Resizing windows
vim.keymap.set('n', '<C-h>', '<cmd>vertical resize +2<CR>')
vim.keymap.set('n', '<C-j>', '<cmd>resize -2<CR>')
vim.keymap.set('n', '<C-k>', '<cmd>resize +2<CR>')
vim.keymap.set('n', '<C-l>', '<cmd>vertical resize -2<CR>')

-- Toggle theme
vim.keymap.set('n', '<A-t>', '<cmd>lua ToggleBackground()<cr>')

-- Code autocompletion
vim.keymap.set('i', '<C-space>', '<C-x><C-o>')

-- Toggle additiniol functionality
vim.keymap.set('n', '<A-f>', '<cmd>NvimTreeToggle<CR>')
vim.keymap.set('n', '<A-m>', '<cmd>MinimapToggle<CR>')

------------------------------------------------------------------------------

require('lua_config')
