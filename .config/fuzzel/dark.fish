set background_primary      '303030ff'
set background_secondary    '4f4f4fff'
set foreground              'FBFBFEff'
set highlight               'cb4b16ff'

fuzzel -b $background_primary -t $foreground -s $background_secondary -S $foreground -C $highlight -m $highlight -f Cantarell -w 15 -T kitty -i Papirus -B 2
