set background_primary      'ebebebff'
set background_secondary    'fafafaff'
set foreground              '303030ff'
set highlight               'cb4b16ff'

fuzzel -b $background_primary -t $foreground -s $background_secondary -S $foreground -C $highlight -m $highlight -f Cantarell -w 15 -T kitty -i Papirus -B 2
#fuzzel -b $background_primary -t $foreground -s $background_secondary -S $foreground -C $highlight -m $highlight -f Cantarell -w 15 -T kitty -i Papirus -B 2 -y 300 -x 785 -w 15
